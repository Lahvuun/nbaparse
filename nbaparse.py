import argparse
import csv
import logging
import os
import re
from collections import OrderedDict
from pathlib import Path
from threading import Thread


class Game:
    # ~Witchcraft~ Regular expression that correctly matches ~99.9995%
    # of player names.
    name_re = re.compile(r'[A-Z]\. \w+(?:(?:-|\')\w+| [A-Z]\w+)*')

    def __init__(self, desc):
        players = {}

        i = 0
        for quarter_num in range(4):
            quarter_desc = []
            while desc[i][1] != '-':
                quarter_desc.append(desc[i])
                i += 1
            i += 2

            actions = [row[5] for row in quarter_desc]
            times = [row[0] for row in quarter_desc]
            score_changes = [row[4] for row in quarter_desc]
            for idx, ch in enumerate(score_changes):
                # [-1] because sometimes change has two symbols,
                # first being garbage.
                # Check for numeric because without score change
                # value is either empty or gibberish.
                score_changes[idx] = int(ch[-1]) if ch[-1].isnumeric() else 0
            names_lists = [Game.name_re.findall(act) for act in actions]

            for names in names_lists:
                if names:
                    for name in names:
                        if name not in players:
                            players[name] = Player(name)
                        player = players[name].quarters[quarter_num]
                        player['started_at'] = 12 * 60

            superlist = zip(actions, score_changes, times, names_lists)
            for action, score_change, action_time, names in superlist:
                if names:
                    first_player = players[names[0]].quarters[quarter_num]
                    second_player = None
                    if len(names) > 1:
                        second_player = players[names[1]].quarters[quarter_num]
                    # Where the magic happens:
                    # Each action string has a unique word to it.
                    # This, together with other attributes (like score change
                    # and amount of players in action), allows us
                    # to classify easily.
                    if 'rebound' in action:
                        first_player['rebounds'] += 1

                    elif 'free' in action:
                        idx = action.find(' of ')
                        count = int(action[idx + 4]) if idx > -1 else 1
                        if score_change:
                            first_player['points'] += 1
                        else:
                            first_player['missed_fs'] += 1 / count

                    elif 'shot' in action:
                        point = int(action[action.find('-pt') - 1])
                        if score_change:
                            first_player['points'] += score_change
                            if second_player:
                                second_player['assists'] += 1
                        else:
                            t_or_g = 't' if point == 2 else 'g'
                            missed_f = 'missed_f' + t_or_g
                            first_player[missed_f] += 1
                            if second_player:
                                second_player['blocks'] += 1

                    elif 'Turnover' in action:
                        first_player['turnovers'] += 1
                        if second_player:
                            second_player['steals'] += 1

                    elif 'enters' in action:
                        time = action_time.split(':')
                        minutes = int(time[0])
                        seconds = int(float(time[1]))
                        time_sec = minutes * 60 + seconds
                        sp_delta = second_player['started_at'] - time_sec
                        second_player['seconds'] += sp_delta
                        second_player['played_until_end'] = False
                        first_player['started_at'] = time_sec
                        first_player['played_until_end'] = True

                    elif 'foul' in action:
                        first_player['fouls'] += 1

                    else:
                        logging.warning('Unknown action: "{}"'.format(action))
                else:
                    logging.warning('Unknown action: "{}"'.format(action))

            # Let's not forget to calculate playtimes of those
            # who didn't get benched.
            for p in players.values():
                p = p.quarters[quarter_num]
                if p['played_until_end']:
                    p['seconds'] += p['started_at']

        self.players = players

    def get_quarter_stats_csv(self, quarter_num):
        q = 'per Quarter {}'.format(quarter_num + 1)
        csv = [[
            'player',
            'points ' + q,
            'rebounds ' + q,
            'assists ' + q,
            'steals ' + q,
            'blocks ' + q,
            'fouls ' + q,
            'Missed FG ' + q,
            'Missed FT ' + q,
            'Missed FS ' + q,
            'TO ' + q,
            'min ' + q,
            'efficiency',
        ]]
        players = self.players
        players_csv = []
        for p in players:
            if players[p].quarters[quarter_num]['seconds']:
                players_csv.append(players[p].get_quarter_stats(quarter_num))
        return csv + sorted(players_csv)

    def get_match_stats_csv(self):
        csv = [[
                'player',
                'efficiency Quarter 1',
                'efficiency Quarter 2',
                'efficiency Quarter 3',
                'efficiency Quarter 4',
        ]]
        players = self.players
        stats = []
        for p in players.values():
            player_stats = [p.name]
            player_stats += [p.get_efficiency_for_quarter(q) for q in range(4)]
            stats.append(player_stats)

        return csv + sorted(stats)


class Player:
    def __init__(self, name):
        self.name = name
        report_fields = (
            'points',
            'rebounds',
            'assists',
            'steals',
            'blocks',
            'fouls',
            'missed_fg',
            'missed_ft',
            'missed_fs',
            'turnovers',
            'minutes',
        )
        initial_values = list(zip(report_fields, (0,) * len(report_fields)))
        initial_values += [
            ('seconds', 0),
            ('started_at', 0),
            ('played_until_end', True),
        ]

        quarters = [dict(initial_values) for _ in range(4)]
        self.report_fields = report_fields
        self.quarters = quarters

    def get_quarter_stats(self, quarter_num):
        q = self.quarters[quarter_num]
        q['minutes'] = round(q['seconds'] / 60, 2)
        stats = ([self.name]
                 + [q[rf] for rf in self.report_fields]
                 + [self.get_efficiency_for_quarter(quarter_num)]
                 )
        return stats

    def get_efficiency_for_quarter(self, quarter):
        q = self.quarters[quarter]
        if not q['seconds']:
            return 0

        return (q['points']
                + q['rebounds']
                + q['assists']
                + q['steals']
                + q['blocks']
                - q['fouls']
                - q['missed_fg']
                - q['missed_ft']
                - q['missed_fs']
                - q['turnovers']) / (q['seconds'] / 60)


def extract_games_from_dir(path):
    games = []

    for game_path in path.iterdir():
        with open(game_path, encoding='iso-8859-1') as game_csv:
            reader = csv.reader(game_csv)
            # First five rows are irrelevant (column names, teams, etc.)
            desc = list(reader)[5:]
            game = Game(desc)
        name = os.path.split(game_path)[1]
        name = os.path.splitext(name)[0]
        game.name = name
        games.append(game)
    return games


def write_games_to_disk(games, path):
    for game in games:
        game_path = os.path.join(path, game.name)
        os.makedirs(game_path, exist_ok=True)
        for quarter_idx in range(4):
            quarter_name = '{}st quarter.csv'.format(quarter_idx + 1)
            quarter_file_path = os.path.join(game_path, quarter_name)
            # newline='' avoids spawning \r\r\n on Windows.
            with open(quarter_file_path, mode='w', newline='') as quarter_file:
                writer = csv.writer(quarter_file)
                writer.writerows(game.get_quarter_stats_csv(quarter_idx))
        players_file_path = os.path.join(game_path, 'players.csv')
        with open(players_file_path, mode='w', newline='') as players_file:
            writer = csv.writer(players_file)
            writer.writerows(game.get_match_stats_csv())


def write_players_to_disk(games, path):
    players = OrderedDict()
    for game in games:
        for name, player in game.players.items():
            if name not in players:
                players[name] = []
            quarterly_eff = []
            for q_num in range(len(player.quarters)):
                quarterly_eff.append(player.get_efficiency_for_quarter(q_num))
            players[name].append(quarterly_eff + [game.name])
    for player, stats in players.items():
        total_stats_file_path = os.path.join(path, player + '.csv')
        with open(total_stats_file_path, mode='w', newline='') as player_file:
            writer = csv.writer(player_file)
            writer.writerow([
                'player',
                'efficiency Quarter 1',
                'efficiency Quarter 2',
                'efficiency Quarter 3',
                'efficiency Quarter 4',
                'play'
            ])
            writer.writerow([player] + stats[0])
            for row in stats[1:]:
                writer.writerow([None] + row)


if __name__ == '__main__':
    program_desc = ('Parse and analyze the "games" directory with '
                    '.csv files about NBA games, then output to the '
                    '"results" and "players" directories.')
    parser = argparse.ArgumentParser(description=program_desc)
    parser.parse_args()

    logging.basicConfig(filename='nbaparse.log', level=logging.DEBUG)

    work_dirs = {
            'games': Path('games'),
            'results': Path('results'),
            'players': Path('players'),
            # 'normalized': Path('normalized players'),
    }
    for d in work_dirs:
        os.makedirs(work_dirs[d], exist_ok=True)

    games = extract_games_from_dir(work_dirs['games'])

    results_thread = Thread(target=write_games_to_disk,
                            args=(games, work_dirs['results']))
    players_thread = Thread(target=write_players_to_disk,
                            args=(games, work_dirs['players']))
    results_thread.start()
    players_thread.start()
